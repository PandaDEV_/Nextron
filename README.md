<img src="https://cdn.discordapp.com/attachments/903181864116826112/1085873702295588874/Nextron_Banner.png" />


**IMPORTANT: This plugin is renamed to Nextron and was EssentialsP before!**

**Thanks so much for 300 downloads. ♥♥♥**

[Website >](https://essentialsp.tk/)

<details>
<summary>
🛠️ Testing
</summary>
<span>If you want to help me test my plugin, join the <a href="https://discord.gg/Y7SbYphVw9" >discord.</a></span>
</details>

***

<img src="https://cdn.discordapp.com/attachments/903181864116826112/1043082635221684295/About_purple.png" height="54px"/>

Nextron is a powerful Minecraft plugin which is meant to replace any other plugins you use at the moment. It has beautiful GUI's that simplifies the process to many users because you don't have to use complicated commands. 

***

<img src="https://cdn.discordapp.com/attachments/903181864116826112/1043087096182734859/Requirements_purple.png" height="54px"/>

- A permission plugin is highly recommended, for example [LuckPerms](https://luckperms.net/), as it can manage all permissions.
- Nextron runs on Bukkit, Spigot, Paper and [Purpur (recommended)](https://purpurmc.org/).
- Nextron supports 1.13 and higher.

***

<img src="https://cdn.discordapp.com/attachments/903181864116826112/1043082636240900147/Issue_purple.png" height="54px"/>

If you have any issues or find a bug, please remember to report it here [GitHub](https://github.com/0PandaDEV/Nextron/issues)

***

<img src="https://cdn.discordapp.com/attachments/903181864116826112/1043082635771134002/Credits_purple.png" height="54px"/>

[Negative Games](https://github.com/Negative-Games/Framework) Dependency by NegativeDev, seailz and joeecodes <br>
[triumph-gui](https://github.com/TriumphTeam/triumph-gui) by triumph team <br>
[languify](https://github.com/Hekates/Languify) by Hekates

***

Check out my other projects on [my profile](https://modrinth.com/user/PandaDEV)
